// Spectrometer functionality script

$(document).ready(function() {
    // Use a "/test" namespace.
    // An application can open a connection on multiple namespaces, and
    // Socket.IO will multiplex all those connections on a single
    // physical channel. If you don't care about multiple channels, you
    // can set the namespace to an empty string.
    namespace = '/spectrometer';

    // Connect to the Socket.IO server.
    // The connection URL has the following format:
    //     http[s]://<domain>:<port>[/<namespace>]
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace);

    // Event handler for new connections.
    // The callback function is invoked when a connection with the
    // server is established.
    //socket.on('connect', function() {
    //    socket.emit('my_event', {data: 'I\'m connected!'});
    //});

    // Event handler for server sending spectrometer data - update graph with new data
    socket.on("send_spectrum", function(msg) {
        myChart.data = msg.data;
        myChart.update();
    });

    // Handler for "Get spectrum" button
    $('form#request').submit(function(event) {
        socket.emit("request_spectrum");
        return false;
    });

    var ctx = document.getElementById("spectrumChart").getContext('2d');

    var myChart = new Chart(ctx, {
        type: "line",
        data: {
            datasets: [{
                data: [0, 10, 20, 50, 0, 70, 20, 10, 20, 10]
            }],

            labels: [400, 430, 460, 490, 520, 550, 580, 610, 640, 670]
        },

        options: {
            scales: {
                xAxes: {
                    ticks: {
                        min: 400,
                        max: 700
                    }
                }
            }
        }
    });
});

