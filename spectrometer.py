from threading import Lock
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit
from random import randint
import json
from get_spectrum import spectrum_live_graph as sp

app = Flask(__name__)
app.config["SECRET_KEY"] = "Super secret!"
socketio = SocketIO(app, async_mode=None)

thread = None
thread_lock = Lock()
cam = None
image_size = (1920, 640)
roi = ((488, 134), (1557, 150))

@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html", async_mode=socketio.async_mode)


@app.route("/live")
@app.route("/livesettings")
@app.route("/save")
def live_spectrum():
    return render_template("live.html", async_mode=socketio.async_mode)


@app.route("/testui")
@app.route("/tests")
def tests():
    return render_template("tests.html", async_mode=socketio.async_mode)


@socketio.on("request_spectrum", namespace='/spectrometer')
def sendSpectrum():
    session['receive_count'] = session.get('receive_count', 0) + 1

    dummy_spectrum = dummySpectrum();
    print(dummy_spectrum)

    emit("send_spectrum",
         {"data": dummy_spectrum})


@socketio.on("connect", namespace="/spectrometer")
def connect():
    global thread
    global cam
    with thread_lock:
        if thread is None:
            cam = sp.startCamera(image_size, verbose=True)
            cam.iso = 400
            cam.shutter_speed = 250000
            thread = socketio.start_background_task(target=spectrumUpdateThread)

    # Emit status update if implemented later
    #emit("status", {"data": "connected"})


def dummySpectrum():
    """
    Generate a randomised dummy spectrum
    """

    spectrum = [randint(0, 100) for _ in range(10)]
    wavelengths = [i for i in range(400, 700, 30)]

    return {
        "datasets": [{
            "data": spectrum
        }],
        "labels": wavelengths
    }


def getSpectrum():
    """
    Take an image and return its spectrum
    """
    global cam

    img = sp.getImgData(cam, image_size, roi, verbose=True)
    spectrum = sp.averageCols(img)
    wavelengths = [i for i, _ in enumerate(spectrum)]

    return {
        "datasets": [{
            "data": spectrum
        }],
        "labels": wavelengths
    }


def spectrumUpdateThread():
    """
    Sends server-generated spectrum updates to clients
    """

    while True:
        socketio.sleep(1)
        socketio.emit("send_spectrum",
                      {"data": getSpectrum()},
                      namespace="/spectrometer")


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", debug=True)
